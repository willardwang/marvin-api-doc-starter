module.exports = {
    extends: ['@commitlint/config-conventional'],
    rules: {
        'type-enum': [2, 'always', [
            "feat", "fix", "docs", "style", "refactor", "perf", "test", "build", "ci", "chore", "revert"
        ]],
        'scope-case': [2, 'always', ["pascal-case", "lower-case", "camel-case"]],
        'scope-empty': [2, 'never'],
        'subject-full-stop': [0, 'never'],
        'subject-case': [0, 'never']
    }
};