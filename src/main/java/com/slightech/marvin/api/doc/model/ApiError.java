package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author willardwang
 * @description
 * @date 2019/08/05
 */
@Getter
@Setter
public class ApiError {
    private String code;
    private String msg;
    private String description;
}
