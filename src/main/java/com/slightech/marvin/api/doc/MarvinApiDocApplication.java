package com.slightech.marvin.api.doc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author  willardwang
 *
 */
@SpringBootApplication
public class MarvinApiDocApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarvinApiDocApplication.class, args);
    }

}
