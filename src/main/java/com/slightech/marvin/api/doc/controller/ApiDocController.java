package com.slightech.marvin.api.doc.controller;

import com.slightech.marvin.api.doc.autoconfigure.ApiDocScanner;
import com.slightech.marvin.api.doc.model.ApiDocInfo;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author willardwang
 * @description
 * @date 2019/07/29
 */
@Controller
@RequestMapping("apiDoc")
public class ApiDocController {

    private Logger logger = LoggerFactory.getLogger(ApiDocController.class);

    @Autowired
    private ApiDocScanner apiDocScanner;
    @Autowired
    private ApiDocInfo apiDocInfo;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        logger.debug("service:{}", apiDocScanner);
        val apiGroup = apiDocScanner.getApiGroupList();
        model.addAttribute("apiDocInfo", apiDocInfo);
        model.addAttribute("apiGroupList", apiGroup);
        model.addAttribute("dictionaryDocMap", apiDocInfo.getDictionaryDocMap());
        return "index";
    }

    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public String detail(@RequestParam Integer id, Model model) {
        logger.info("请求API ID:{}", id);
        val allApi = apiDocScanner.getAllApi();
        val api = allApi.get(id);
        model.addAttribute("api", api);
        return "detail";
    }

    @RequestMapping(value = "home", method = RequestMethod.GET)
    public String dictionaryDoc(Model mode) {
        val apiCounterList = apiDocScanner.getApiCountList();
        mode.addAttribute("apiDocInfo", apiDocInfo);
        mode.addAttribute("apiCounterList", apiCounterList);
        return "home";
    }

    @RequestMapping(value = "dictionaryDoc", method = RequestMethod.GET)
    public String home(@RequestParam Integer id, Model model) {
        logger.info("请求doc ID:{}", id);
        val dictionaryDocMap = apiDocInfo.getDictionaryDocMap();
        val dictionaryDoc = dictionaryDocMap.get(id);
        model.addAttribute("dictionaryDoc", dictionaryDoc);
        return "dictionaryDoc";
    }

}
