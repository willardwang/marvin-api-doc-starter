package com.slightech.marvin.api.doc.annotation;

import com.slightech.marvin.api.doc.model.SupportClientType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MarvinApi {
    String value() default "";

    String description() default "";

    SupportClientType[] supportClientType() default {};
}
