package com.slightech.marvin.api.doc.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author willardwang
 * @description
 * @date 2019/07/29
 */
@ConfigurationProperties(prefix = "marvin.api.doc")
public class ApiDocProperties {

    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
