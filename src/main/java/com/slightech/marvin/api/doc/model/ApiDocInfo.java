package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author willardwang
 * @description
 * @date 2019/08/05
 */
@Getter
@Setter
public class ApiDocInfo {
    private String title;
    private String description;
    private List<TeamMember> teamMemberList;
    private Map<Integer, DictionaryDoc> dictionaryDocMap = new HashMap<>();

    private ApiDocInfo(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public static class ApiDocInfoBuilder {
        private String title;
        private String description;
        private List<TeamMember> teamMembers = new ArrayList();
        private Map<Integer, DictionaryDoc> dictionaryDocMap = new HashMap();
        private int dictironaryId = 0;

        public ApiDocInfoBuilder title(String title) {
            this.title = title;
            return this;
        }

        public ApiDocInfoBuilder description(String description) {
            this.description = description;
            return this;
        }

        public ApiDocInfoBuilder addTeamMember(String name, String email) {
            teamMembers.add(new TeamMember(name, email));
            return this;
        }

        public ApiDocInfoBuilder addDictionaryDoc(DictionaryDoc dictionaryDoc) {
            dictironaryId++;
            dictionaryDocMap.put(dictironaryId, dictionaryDoc);
            return this;
        }

        public ApiDocInfo build() {
            ApiDocInfo apiDocInfo = new ApiDocInfo(this.title, this.description);
            apiDocInfo.setTeamMemberList(this.teamMembers);
            apiDocInfo.setDictionaryDocMap(dictionaryDocMap);
            return apiDocInfo;
        }
    }
}
