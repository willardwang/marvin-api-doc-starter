package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author willardwang
 * @description
 * @date 2019/07/31
 */
@Getter
@Setter
@ToString
public class ApiField {
    private String fieldName;
    private String example;
    private String type;
    private boolean required;
    private String description;
}
