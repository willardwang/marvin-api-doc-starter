package com.slightech.marvin.api.doc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MarvinApiResponseParam {
    MarvinApiParamField[] field() default {};

    MarvinApiResponseClass responseClass() default @MarvinApiResponseClass;

    MarvinApiParamError[] error() default {};
}