package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author willardwang
 * @description
 * @date 2019/08/05
 */
@Getter
@Setter
public class TeamMember {
    private String name;
    private String email;

    public TeamMember(String name, String email) {
        this.name = name;
        this.email = email;
    }
}
