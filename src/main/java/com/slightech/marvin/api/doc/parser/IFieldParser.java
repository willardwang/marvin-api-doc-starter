package com.slightech.marvin.api.doc.parser;

import com.slightech.marvin.api.doc.annotation.MarvinApiGenericField;
import com.slightech.marvin.api.doc.annotation.MarvinApiParamField;

/**
 * @author willardwang
 * @description
 * @date 2019/08/02
 */
public interface IFieldParser {
    /**
     * 解析类的字段属性
     * @param clz
     * @return
     */
     FieldCollection parseClazzField(Class clz);

    /**
     *解析带有不确定泛型的类
     * @param clz 解析类
     * @param marvinApiGenericFields 泛型标注字段
     * @return
     */
     FieldCollection parseClazzField(Class clz, MarvinApiGenericField[] marvinApiGenericFields);

    /**
     * 解析字段
     * @param marvinApiParamFields
     * @return
     */
     FieldCollection parseMarvinApiParamField(MarvinApiParamField[] marvinApiParamFields);
}
