package com.slightech.marvin.api.doc.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author willardwang
 * @description
 * @date 2019/08/01
 */
public class StringTool {

    private static Pattern humpPattern = Pattern.compile("[A-Z]");

    /**
     * 驼峰转下划线
     * @param str
     * @return
     */
    public static String humpToLine2(String str) {
        Matcher matcher = humpPattern.matcher(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, "_" + matcher.group(0).toLowerCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}
