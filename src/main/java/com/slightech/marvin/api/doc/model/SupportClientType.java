package com.slightech.marvin.api.doc.model;

import lombok.Getter;

/**
 * @author willardwang
 * @description 支持调用端
 * @date 2019/07/29
 */
@Getter
public enum SupportClientType {

    ROBOT_CLIENT("机器人客户端", "robot_client"),
    APP("APP应用", "app"),
    WEB("WEB后台", "web"),
    WXAPP("小程序", "wxapp"),
    INNER_SERVICE("服务内部", "inner_service");

    private String name;
    private String code;

    SupportClientType(String name, String code) {
        this.name = name;
        this.code = code;
    }
}
