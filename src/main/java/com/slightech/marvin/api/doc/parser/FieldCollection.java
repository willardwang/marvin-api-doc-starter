package com.slightech.marvin.api.doc.parser;

import com.slightech.marvin.api.doc.model.ApiField;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author willardwang
 * @description
 * @date 2019/08/02
 */
@Getter
@Setter
public class FieldCollection {
    private List<ApiField> apiFieldList;
    private Map<String, Object> apiFieldMap;
}
