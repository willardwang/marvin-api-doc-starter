package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.Objects;

/**
 * @author willardwang
 * @description
 * @date 2019/07/29
 */
@Getter
@Setter
public class ApiGroup implements Comparable<ApiGroup>{
    private String nameSpaceUri;
    private String name;
    private String description;
    private Map<Integer, Api> apiMap;
    private int order;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiGroup api = (ApiGroup) o;
        return Objects.equals(name, api.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(ApiGroup o) {
        if (o.getOrder() == this.getOrder()) {
            return o.getName().compareTo(this.getName());
        }
        return o.getOrder() - this.order;
    }
}
