package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

import java.util.ArrayList;
import java.util.List;

/**
 * @author willardwang
 * @description
 * @date 2019/08/06
 */
@Getter
@Setter
public class DictionaryDoc {
    private String name;
    private String description;
    private String tableTitleName;
    private String tableValueName;
    private String tableDescriptionName;
    private List<Dictionary> dictionaryList = new ArrayList<>();

    private DictionaryDoc(DictionaryDocBuilder builder) {
        this.name = builder.name;
        this.description = builder.description;
        this.tableTitleName = builder.tableTitleName;
        this.tableValueName = builder.tableValueName;
        this.tableDescriptionName = builder.tableDescriptionName;
    }

    public static class DictionaryDocBuilder {
        private String name;
        private String description;
        private String tableTitleName;
        private String tableValueName;
        private String tableDescriptionName;
        private List<Dictionary> dictionaryList = new ArrayList<>();

        public DictionaryDocBuilder name(String name) {
            this.name = name;
            return this;
        }

        public DictionaryDocBuilder description(String description) {
            this.description = description;
            return this;
        }

        public DictionaryDocBuilder tableTitleName(String tableTitleName) {
            this.tableTitleName = tableTitleName;
            return this;
        }

        public DictionaryDocBuilder tableValueName(String tableValueName) {
            this.tableValueName = tableValueName;
            return this;
        }

        public DictionaryDocBuilder tableDescriptionName(String tableDescriptionName) {
            this.tableDescriptionName = tableDescriptionName;
            return this;
        }

        public DictionaryDocBuilder addDictionary(Dictionary dictionary) {
            dictionaryList.add(dictionary);
            return this;
        }

        public DictionaryDoc build() {
            val dictionaryDoc = new DictionaryDoc(this);
            dictionaryDoc.setDictionaryList(this.dictionaryList);
            return dictionaryDoc;
        }
    }
}
