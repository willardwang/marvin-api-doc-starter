package com.slightech.marvin.api.doc.autoconfigure;

import com.slightech.marvin.api.doc.model.ApiDocInfo;
import com.slightech.marvin.api.doc.properties.ApiDocProperties;
import lombok.val;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author willardwang
 * @description
 * @date 2019/07/29
 */
@Configuration
@EnableConfigurationProperties({ApiDocProperties.class})
@ConditionalOnClass({ApiDocScanner.class, ApiDocInfo.class})
public class MarvinApiDockAutoConfigure {
    @Autowired
    private ApiDocProperties apiDocProperties;
    private Logger logger = LoggerFactory.getLogger(MarvinApiDockAutoConfigure.class);

    @Bean
    @ConditionalOnMissingBean(ApiDocScanner.class)
    public ApiDocScanner scanner() {
        logger.info("Marvin ApiDoc ApiDocScanner init");
        val apiDocScanner = new ApiDocScanner();
        apiDocScanner.setEnabled(apiDocProperties.isEnabled());
        return apiDocScanner;
    }

    @Bean
    @ConditionalOnMissingBean(ApiDocInfo.class)
    public ApiDocInfo apiDocInfo() {
        logger.info("Marvin ApiDoc info");
        val apiDocInfo = new ApiDocInfo
                 .ApiDocInfoBuilder()
                 .title("Marvin api Doc")
                 .description("自动文档生成")
                 .build();
        return apiDocInfo;
    }
}
