package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author willardwang
 * @description
 * @date 2019/08/16
 */
@Getter
@Setter
public class ApiCount implements Comparable<ApiCount> {
    private String key;
    private String name;
    private Integer count;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApiCount apiCount = (ApiCount) o;
        return Objects.equals(key, apiCount.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public int compareTo(ApiCount o) {
        if (o.count.equals(this.count)) {
            return this.key.compareTo(o.key);
        }
        return o.count - this.count;
    }
}
