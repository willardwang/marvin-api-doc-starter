package com.slightech.marvin.api.doc.parser;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author willardwang
 * @description
 * @date 2019/08/05
 */
@Getter
@Setter
@ToString
public class DocField {
    private String realType;
    private String showType;
    private Object exampleValue;
}
