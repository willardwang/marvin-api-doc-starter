package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author willardwang
 * @description
 * @date 2019/07/29
 */
@Getter
@Setter
public class Api {
    private int id;
    private String name;
    private String description;
    private ApiRequest request;
    private ApiResponse response;
}
