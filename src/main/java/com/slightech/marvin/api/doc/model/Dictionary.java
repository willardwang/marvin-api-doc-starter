package com.slightech.marvin.api.doc.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author willardwang
 * @description
 * @date 2019/08/06
 */
@Getter
@Setter
public class Dictionary {
    private String name;
    private String value;
    private String description;
}
