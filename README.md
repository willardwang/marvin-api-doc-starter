# Springboot项目API文档自动生成框架

## 框架说明

### 微服务集成
 1. pom.xml 文件引入依赖即可使用

 ```
    <dependency>
            <groupId>com.slightech.marvin.api</groupId>
            <artifactId>marvin-api-doc-starter</artifactId>
            <version>0.0.1-SNAPSHOT</version>
   </dependency>

 ```


2. 生成文档开启配置

```
使用项目中加入即可开启文档自动生成
marvin.api.doc.enabled = true

```

3.配置文档自定义说明

自定义配置，可以配置全局字典，成员，文档说明



```

@Configuration
public class MarvinApiDocConfig {
    @Bean
    public ApiDocInfo apiDocInfo(){
        val dictionaryDocBuilder = new DictionaryDoc
                            .DictionaryDocBuilder()
                            .name("异常错误")
                            .description("错误")
                            .tableTitleName("错误码")
                            .tableValueName("错误提示")
                            .tableDescriptionName("描述");


        for (ResultCode resultCode : ResultCode.values()) {
            val dictionary =  new Dictionary();
            dictionary.setName(resultCode.code());
            dictionary.setValue(resultCode.msg());
            dictionary.setDescription(resultCode.msg());
            dictionaryDocBuilder.addDictionary(dictionary);
        }

        val dictionaryDocOuterBuilder = new DictionaryDoc
                .DictionaryDocBuilder()
                .name("全局异常")
                .description("错误")
                .tableTitleName("错误码")
                .tableValueName("错误提示")
                .tableDescriptionName("描述");

        for (ReturnCode returnCode : ReturnCode.values()) {
            val dictionary =  new Dictionary();
            dictionary.setName(returnCode.getCode());
            dictionary.setValue(returnCode.getMsg());
            dictionary.setDescription(returnCode.getMsg());
            dictionaryDocOuterBuilder.addDictionary(dictionary);
        }

        return new ApiDocInfo.ApiDocInfoBuilder()
                .title("访客服务接口")
                .description("<p>我是访客接口文档</p>")
                .addTeamMember("coder1", "coder1@slightech")
                .addTeamMember("coder2", "coder2@slightech11")
                .addDictionaryDoc(dictionaryDocOuterBuilder.build())
                .addDictionaryDoc(dictionaryDocBuilder.build())
                .build();
    }
}

```


4.启动springboot服务

访问文档地址 : http://domain:port/apiDoc 即可访问文档



## 使用到的注解说明

* @MarvinApiDoc
* @MarvinApi
* @MarvinApiRequestParam
* @MarvinApiParamField
* @MarvinApiResponseParam
* @MarvinApiGenericField
* @MarvinApiParamError



### 1. @MarvinApiDoc

说明:

```
@MarvinApiDoc 用于注解在Controller 上面， 接口生成会扫描带有Controller上上面的注解。只有带有此注解的接口，
生成接口文档

```
---
列子：

```
@RequestMapping("package")
@RestController
@MarvinApiDoc(value = "套餐", order = 4)
public class CapacityPackageController extends BaseController {

}
```
---
字段解析：

* value:接口组名称，会生成文档左侧的分类组名称
* order: 文档左侧接口从上到下排序， order 值越大，越靠前
* description: 接口组描述说明

### 2. @MarvinApi

说明：

```
@MarvinApi 用于注解在Controller 内的接口方法上面，接口方法上面标记此注释也可以生成文档，
默认会根据@RequestMapping 注释生成文档。@MarvinApi 此注解主要针对接口方法进行描述说明，和中文命名，更容易阅读

```
---

例子

```
@MarvinApi(value = "套餐初始化", description = "用于后台调用套餐初始接口"，supportClientType={SupportClientType.APP, SupportClientType.WEB} )
@RequestMapping(value = "init", method = RequestMethod.POST)
 public String initBuildingPackage(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME)
    return buildResponseString(logger, ResultCode.SUCCESS);
}

```

字段解析：

* value:接口名称
* description: 此接口描述说明
* supportClientType: 枚举类型（SupportClientType），支持 调用的客户端




### 3. @MarvinApiRequestParam

说明：

```
@MarvinApiRequestParam 用于注解在Controller 内的接口方法上面，针对接口请求参数进行描述说明。
如果接口方法是自定义的java Bean 自动转换对象，不需要用此注解进行标注。
文档扫描器会自动对javabean 进行字段扫描解析，生成接口参数和列子说明。
即：@MarvinApiRequestParam  用于，非javabean 接口入参的接口描述注解 ， 需要跟 @MarvinApiParamField
一起对字段进行标准使用

```
---

例子

```
 @MarvinApi(value = "获取申请信息")
    @MarvinApiRequestParam( field = {
            @MarvinApiParamField(name = "union_id", example = "o3iamjg7wPdNPO_uvqFM4cOr2p4w", description = "用户 union_id"),
            @MarvinApiParamField(name = "apply_id", type = "int", example = "1", description = "申请ID", required = false),
    })
    @RequestMapping(value = "get_apply_join", method = RequestMethod.POST)
    public String getApplyJoinInfo(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) Map<String, Object> params) throws IOException {
        return “”；
    }

```

字段解析：

* field: 字段 @MarvinApiParamField


### 4. @MarvinApiParamField

说明：

```
@MarvinApiParamField 用于对字段解析自定义标注解析， 可以与@MarvinApiRequestParam 和@MarvinApiResponseParam进行接口字段描述。也可以注解自动转换的的javabean 上
```
---

与@MarvinApiRequestParam 自定义注解例子

```
 @MarvinApi(value = "获取申请信息")
    @MarvinApiRequestParam( field = {
            @MarvinApiParamField(name = "union_id", example = "o3iamjg7wPdNPO_uvqFM4cOr2p4w", description = "用户 union_id"),
            @MarvinApiParamField(name = "apply_id", type = "int", example = "1", description = "申请ID", required = false),
    })
    @RequestMapping(value = "get_apply_join", method = RequestMethod.POST)
    public String getApplyJoinInfo(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) Map<String, Object> params) throws IOException {
      return "";
    }

```

---

与@MarvinApiResponseParam 注解列子

```
  @MarvinApi(value = "获取申请信息", description = "2222")
    @MarvinApiRequestParam( field = {
            @MarvinApiParamField(name = "union_id", example = "222", description = "用户 union_id"),
            @MarvinApiParamField(name = "apply_id", type = "int", example = "222", description = "申请ID", required = false),
    })
    @MarvinApiResponseParam(field = {
            @MarvinApiParamField(name = "user_id", example = "1", description = "用户id"),
            @MarvinApiParamField(name = "apartment_id", type = "int", example = "1", description = "公寓ID", required = false),
    })
    @RequestMapping(value = "get_apply_join", method = RequestMethod.POST)
    public String getApplyJoinInfo(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) Map<String, Object> params) throws IOException {

        return "";
    }

```
与java bean 属性字段 注解列子

```
  @MarvinApiParamField( description = "套餐功能服务")
  private List<CapacityPackageService> services;

```

字段解析：

```
* name: 字段名称
* example： 请求或者返回例子数据
* type: 字段类型  int string, boolean,file(文件类型), List<String>, List<Integer>
* description： 字段描述说明
* required: 是否必传或者一定返回gi
```



### 5. @MarvinApiResponseParam

说明：

```
@ MarvinApiResponseParam 用于注解在Controller 内的接口方法上面，针对返回参数进行描述说明。
如果接口返回的是 java Bean 自动转换对象，需要填充responseClass
如果不是java bean  对象，跟MarvinApiParamField 进行描述返回


```
---

自定义属性返回例子

```
  @MarvinApi(value = "获取申请信息", description = "2222")
    @MarvinApiRequestParam( field = {
            @MarvinApiParamField(name = "union_id", example = "222", description = "用户 union_id"),
            @MarvinApiParamField(name = "apply_id", type = "int", example = "222", description = "申请ID", required = false),
    })
    @MarvinApiResponseParam(field = {
            @MarvinApiParamField(name = "user_id", example = "1", description = "用户id"),
            @MarvinApiParamField(name = "apartment_id", type = "int", example = "1", description = "公寓ID", required = false),
    })
    @RequestMapping(value = "get_apply_join", method = RequestMethod.POST)
    public String getApplyJoinInfo(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) Map<String, Object> params) throws IOException {

        return "";
    }

```

返回是自定义bean 对象列子

```
    @RequestMapping(value = "get_pay_list", method = RequestMethod.POST)
    @MarvinApiResponseParam(responseClass = @MarvinApiResponseClass(clazz = GetPayListResponse.class))
    public String getCapacityPayList(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) GetCapacityPayListDTO capacityPayListDTO)
            throws IOException {

 return "";

    }

   @MarvinApiResponseClass 对象中clazz 指定返回的bean类对象即可
```

---
返回是自定义bean 对象中，有泛型类

```
public class GetPageDataListResponse<T> {
    @JsonProperty("total_page")
    private int totalPage;
    @JsonProperty("total_count")
    private int totalCount;
    @JsonProperty("data")
    private List<T> data;
   }


    @MarvinApi("获取套餐列表")
    @RequestMapping(value = "admin_get_list", method = RequestMethod.POST)
    @MarvinApiResponseParam(responseClass = @MarvinApiResponseClass(clazz = GetPageDataListResponse.class,
            genericField = {
            @MarvinApiGenericField(fildName = "data", filedClass = CapacityPackageVO.class)
            }))
    public String getList(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) GetCapacityPackageListDTO getCapacityPackageListDTO) throws IOException {
       return ""
    }

   @MarvinApiResponseClass 对象中clazz 指定返回的bean类对象即可, 但是返回类有个泛型字段，需要我们标准出来 genericField @MarvinApiGenericField用于标注返回clazz 类中，哪些属性是泛型类型，泛型类的绑定的类什是什么, 标准后，就会自动解析返回对象和生成列子json
```
---

返回中标注错误信息列子

```
@RequestMapping(value = "get_info", method = RequestMethod.POST)
    @MarvinApi("获取公司信息")
    @MarvinApiResponseParam(responseClass = @MarvinApiResponseClass(clazz = CompanyInfoVO.class), error = {
            @MarvinApiParamError(code = "123", description = "啦啦",  msg = "错误"),
            @MarvinApiParamError(code = "456", description = "啦啦",  msg = "错误")

    })
    public String getCompanyInfo(@RequestParam(ApiConstant.REQUEST_JSON_PARAM_NAME) Map<String, Object> params) throws IOException {
    }
```
 @MarvinApiParamError 进行描述

字段解析：

* field: 字段 @MarvinApiParamField  自定义返回的对象参数描述
* responseClass  @MarvinApiResponseClass  返回是bean对象
* error @MarvinApiParamError 返回错误描述

### 6. @MarvinApiGenericField

说明

```
  此标准是跟@MarvinApiResponseParam  返回对象bean 中有不确定泛型时，指定泛型属性绑定的class 对象， 详细见：@MarvinApiResponseParam

```

字段解析：

* fieldName:   泛型字段名称
* fieldClass  泛型字段的class 类型

### 7.@MarvinApiParamError

说明

```
  此标准是跟@MarvinApiResponseParam 返回错误码

```

字段解析：
* code:   错误码
* msg ： 错误说明
* description: 错误列子


### 项目发布到私服

```
mvn clean deploy -X -Dmaven.test.skip=true

```

### 6. 生成文档案例如下

![22](src/main/resources/static/img/1.png)


![22](src/main/resources/static/img/2.png)

![22](src/main/resources/static/img/3.png)